'use strict'
const fs = require('fs')
const sleep = require('sleep');
const readline = require('readline');
const rp = require('request-promise');
const $ = require('cheerio');

const grUrl = 'https://meimaris.gr';
const comUrl = 'https://meimaris.com';

// Fetch all products from meimaris.com and place them in a file
const get_URL_com = async () => {
	console.log('Fetching all products from ' + comUrl + '...');

	if (fs.existsSync('./output/products-com.txt')) {
		fs.unlinkSync('./output/products-com.txt');
		fs.openSync('./output/products-com.txt', 'w');
	}
		
	const categories = [];

	await rp(comUrl).then(html => {
		
		const linkObjects = $('tbody > tr > td > p > b > a', html);
		const total = linkObjects.length;

		for (let i = 0; i < total; i++) {
			
			sleep.sleep(1);

			categories.push(linkObjects[i].attribs.href);
		}
		
	}).catch(err => {
		console.log(err);
	});

	await rp(comUrl).then(html => {

		const linkObjects = $('b > a', html);
		const total = linkObjects.length;

		for (let i = 0; i < total; i++) {
			sleep.sleep(1);
			categories.push(linkObjects[i].attribs.href);
		}
		
	}).catch(err => {
		console.log(err);
	});

	categories.forEach(item => {

		sleep.sleep(1);

		rp(`${comUrl}/${item}`).then(html => {

			const products = $('table > tbody > tr > td > center > b > font > a', html);
			const total = products.length;

			for (let i = 0; i < total; i++) {
				console.log(comUrl + '/' + products[i].attribs.href);
				fs.appendFileSync('./output/products-com.txt', comUrl + '/' + products[i].attribs.href + '\n');
			}
			
		}).catch(() => {
			console.log(`ERROR - ${comUrl}/${item}`);
		});
	});
}

// Fetch all products from meimaris.gr and place them in a file
const get_URL_gr = async () => {
	console.log('Fetching all products from ' + grUrl + '...');

	const categories = [
		{
		  	name: 'GPS DEVICES',
		  	url: 'gps-devices',
			pages: 20
		},
		{
		  	name: 'MOBILE TRANSCIEVERS',
		  	url: 'mobile-transceivers',
		  	pages: 10
		},
		{
		  	name: 'FISH FINDERS',
		  	url: 'fishfinders',
		  	pages: 2
		},
		{
		  	name: 'WATCHES',
		  	url: 'rologio-xeiros',
		  	pages: 8
		},
		{
		  	name: 'CAMERAS',
		  	url: 'cameras',
		 	pages: 3
		},
		{
		  	name: 'SMART SHOPPING',
		  	url: 'category-109',
		  	pages: 3
		},
		{
		  	name: 'SALES',
		  	url: 'prosfores',
		  	pages: 1
		}
	];
	  
	if (fs.existsSync('./output/products-gr.txt')) {
		fs.unlinkSync('./output/products-gr.txt');
		fs.openSync('./output/products-gr.txt', 'w');
	}
	  
	categories.forEach(item => {
		let scrapeUrl;
	  
		for (let i = 1; i < item.pages + 1; i++) {
	  		if (i === 1)
				scrapeUrl = `${grUrl}/${item.url}.html`;
		  	else
				scrapeUrl = `${grUrl}/${item.url}-page-${i}.html`;
	  
		  	console.log(scrapeUrl);
	  
		  	rp(scrapeUrl).then(html => {
				const products = $('.product-title', html);
	  
				Object.keys(products).forEach(key => {
			  		const obj = products[key].attribs;
	  
					if (obj) {
						console.log(obj.href);
						fs.appendFileSync('./output/products-gr.txt', obj.href + '\n');
					}
				});
	  
			}).catch(err => {
				console.log(err);
			});
		}
	});
}

// Fetch the SKU code for each product of meimaris.com and place them in a file
const get_SKU_com = async () => {
	console.log('Fetching SKU codes for each product from ' + comUrl + '...');

	if (fs.existsSync('./output/products-sku-com.txt'))
		fs.unlinkSync('./output/products-sku-com.txt');

	const rl = readline.createInterface({
		input: fs.createReadStream('./output/products-com.txt'),
		crlfDelay: Infinity
	});

	for await (const uri of rl) {

		sleep.sleep(1);

		await rp(uri).then(html => {

			const obj = $('table > tbody > tr > td > center > table > tbody > tr:nth-child(2) > td > table > tbody > tr:nth-child(5) > td > table > tbody > tr > td:nth-child(2) > div > table > tbody > tr:nth-child(2) > td > font > span > b:nth-child(4)', html);
			const sku = obj.parent().text().split(':').slice(-1).pop().replace(/\s/g, '');
			if (!sku || sku === '')
				return

			fs.appendFileSync('./output/products-sku-com.txt', sku + '|' + uri + '\n');

			console.log(sku + '|' + uri);
		}).catch(err => {
			console.log(err);
		});
	}
}

// Fetch the SKU code for each product of meimaris.gr and place them in a file
const get_SKU_gr = async () => {
	console.log('Fetching SKU codes for each product from ' + grUrl + '...');

	if (fs.existsSync('./output/products-sku-gr.txt'))
		fs.unlinkSync('./output/products-sku-gr.txt');

	const rl = readline.createInterface({
		input: fs.createReadStream('./output/products-gr.txt'),
		crlfDelay: Infinity
	});

	for await (const uri of rl) {

		sleep.msleep(30);

		await rp(uri).then(html => {

			let sku = $('.ty-control-group__item', html).text();

			if (sku.includes('Άμεσα Διαθέσιμο'))
				sku = sku.replace('Άμεσα Διαθέσιμο', '');
			else if (sku.includes('Σε Απόθεμα'))
				sku = sku.replace('Σε Απόθεμα', '');
			else if (sku.includes('Μη Διαθέσιμο'))
				sku = sku.replace('Μη Διαθέσιμο', '');
			else if (sku.includes('Κατόπιν Παραγγελίας'))
				sku = sku.replace('Κατόπιν Παραγγελίας', '');

			if (!sku || sku === '')
				return
			
			fs.appendFileSync('./output/products-sku-gr.txt', sku + '|' + uri + '\n');
			
			console.log(sku + '|' + uri);
		}).catch(err => {
			console.log('ERROR' + '|' + uri);
		});
	}
}

// Make an index file containing each product in this format: SKU|GR_URL|COM_URL
const join_SKU = async () => {
	console.log('Joining SKU codes...');

	if (fs.existsSync('./output/products-sku-total.txt'))
		fs.unlinkSync('./output/products-sku-total.txt');

	const rlGR = readline.createInterface({
		input: fs.createReadStream('./output/products-sku-gr.txt'),
		crlfDelay: Infinity
	});

	for await (const line of rlGR) {
		let sku = line.split('|')[0];
		let url = line.split('|')[1];

		let lines = fs.readFileSync('./output/products-sku-com.txt', 'utf-8');

		let re = new RegExp('^.*' + sku + '.*$', 'm');
		let m = re.exec(lines);

		if (m) {
			fs.appendFileSync('./output/products-sku-total.txt', sku + '|' + url + '|' + m[0].split('|')[1] + '\n');
			console.log(sku + '|' + url + '|' + m[0].split('|')[1]);
		}
	}
}

// Command handler for npm
(async () => {
	let args = process.argv.slice(2);

	if (args[0] == 'get-products-com')
		await get_URL_com();
	else if (args[0] == 'get-products-gr')
		await get_URL_gr();
	else if (args[0] == 'get-sku-com')
		await get_SKU_com();
	else if (args[0] == 'get-sku-gr')
		await get_SKU_gr();
	else if (args[0] == 'join-sku')
		await join_SKU();
})();